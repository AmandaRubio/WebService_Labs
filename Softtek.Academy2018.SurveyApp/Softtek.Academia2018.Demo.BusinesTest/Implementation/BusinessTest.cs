﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softtek.Academia2018.SurveyApp.Business.Contracts;
using Softtek.Academia2018.SurveyApp.Business.Implementation;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academia2018.SurveyApp.Business.Implementation.Tests
{
    [TestClass()]
    public class BusinessTest
    {
        [TestMethod()]
        public void When_add()
        {
            int expected = 1;

            Mock<IQuestionRepository> repository = new Mock<IQuestionRepository>();

            repository.Setup(x => x.Exist(It.IsAny<int>())).Returns(false);

            IQuestionService service = new QuestionService(repository.Object);

            Question question = new Question
            {
                CreatedDate = DateTime.Now,
                IsActive = true,
                ModifiedDate = DateTime.Now,
                QuestionTypeId = 1,
                Text = "This is the first question"
            };
            int result = service.Add(question);

            Assert.AreEqual(expected, result);
        }

    }
}