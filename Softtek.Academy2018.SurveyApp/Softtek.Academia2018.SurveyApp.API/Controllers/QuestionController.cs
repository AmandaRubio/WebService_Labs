﻿using Softtek.Academia2018.SurveyApp.API.Models;
using Softtek.Academia2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academia2018.SurveyApp.API.Controllers
{
    [RoutePrefix("api/question")]
    public class QuestionController : ApiController
    {
        private readonly IQuestionService _questionService;
        public QuestionController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [Route("{id:int}")]
        [HttpGet]

        public IHttpActionResult Get([FromUri] int id)
        {
            Question question = _questionService.GetId(id);
            if (question == null) return NotFound();

            QuestionDTO questionDTO = new QuestionDTO
            {
                Text = question.Text,
                IsActive = question.IsActive,
                QuestionTypeId = question.QuestionTypeId

            };
            return Ok(questionDTO);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("Request is null");

            Question question = new Question
            {
                Text = questionDTO.Text,
                IsActive = questionDTO.IsActive,
                QuestionTypeId = questionDTO.QuestionTypeId
            };
            int id = _questionService.Add(question);

            if (id <= 0) return BadRequest("Unable to create question");

            var payload = new { QuestionId = id };
            return Ok(payload);
        }
    }
}
