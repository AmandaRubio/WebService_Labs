﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academia2018.SurveyApp.API.Models
{
    public class OptionDTO
    {
        public string Text { get; set; }

        public virtual ICollection<QuestionDTO> Questions { get; set; }
        
    }
}
