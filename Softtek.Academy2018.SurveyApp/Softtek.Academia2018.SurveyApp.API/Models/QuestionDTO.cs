﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academia2018.SurveyApp.API.Models
{
    public class QuestionDTO
    {
        public string Text { get; set; }

        public bool IsActive { get; set; }

        public int QuestionTypeId { get; set; }

        public virtual QuestionTypeDTO QuestionType { get; set; }

        public virtual ICollection<SurveyDTO> Surveys { get; set; }

        public virtual ICollection<OptionDTO> Options { get; set; }
    }
}
