﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academia2018.SurveyApp.API.Models
{
    public class QuestionTypeDTO
    {
        public string Description { get; set; }
    }
}
