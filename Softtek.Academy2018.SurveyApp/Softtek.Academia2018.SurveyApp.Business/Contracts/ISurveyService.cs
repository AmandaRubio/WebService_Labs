﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academia2018.SurveyApp.Business.Contracts
{
    public interface ISurveyService
    {
        //conformada por preguntas 
        bool AddQuestionToSurvey(int surveyId, int questionId);

        ICollection<Question> GetSurveyByQuestion(int questionId);
    }
}
