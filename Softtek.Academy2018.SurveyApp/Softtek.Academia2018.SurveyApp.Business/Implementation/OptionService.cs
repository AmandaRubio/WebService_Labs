﻿using Softtek.Academia2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academia2018.SurveyApp.Business.Implementation
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionService;
        
        public OptionService(IOptionRepository optionService)
        {
            _optionService = optionService;
        }

        public int Add(Option option)
        {
            if (option.Id != 0) return 0;

            if (string.IsNullOrEmpty(option.Text)) return 0;
            
            if(option.Questions.Count != 0) return 0;

            //bool OptionExist = _optionService.Exist(option.Text);

            int id = _optionService.Add(option);
            return id;
        }

        
    }
}
