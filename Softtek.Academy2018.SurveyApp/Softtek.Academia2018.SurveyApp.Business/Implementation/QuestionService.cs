﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academia2018.SurveyApp.Business.Contracts;

namespace Softtek.Academia2018.SurveyApp.Business.Implementation
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;

        public QuestionService(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public int Add(Question question)
        {
            if (string.IsNullOrEmpty(question.Text)) return 0;
            bool isExist = _questionRepository.Exist(question.Id);
            if (!isExist) return 0;
            int id = _questionRepository.Add(question);
            return id;
        }

        public ICollection<Option> GetAll()
        {
            throw new NotImplementedException();
        }

        public Question GetId(int id)
        {
            if (id <= 0) return null;
            Question question = _questionRepository.Get(id);
            if (question != null && !question.IsActive) return null;
            return question;
        }

        public bool Update(Question question)
        {
            if (string.IsNullOrEmpty(question.Text)) return false;
            bool isExist = _questionRepository.Exist(question.Id);
            if (!isExist) return false;

            return _questionRepository.Update(question);
            
        }
    }
}
