﻿using Softtek.Academia2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academia2018.SurveyApp.Business.Implementation
{
    public class QuestionTypeService : IQuestionTypeService
    {
        private readonly IQuestionTypeRepository _qtRepository;
        private readonly IQuestionRepository _qRepository;

        public QuestionTypeService(IQuestionTypeRepository qtRepository, IQuestionRepository qRepository)
        {
            _qtRepository = qtRepository;
            _qRepository = qRepository;
        }
        //public bool AddQuestionTypeToQuestion(int questionTypeId, int questionId)
        //{
        //    if (questionTypeId <= 0 || questionId <= 0) return false;

        //    bool questionTypeExist = _qtRepository.Exist(questionTypeId);
        //    if (!questionTypeExist) return false;

        //    return _qtRepository.Add();
        //}
        
    }
}
