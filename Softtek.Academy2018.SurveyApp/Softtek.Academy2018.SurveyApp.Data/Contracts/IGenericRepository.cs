﻿namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IGenericRepository<T> where T : class
    {
        int Add(T survey);

        T Get(int id);

        bool Update(T survey);

        bool Delete(T survey);

        bool Exist(int id);
    }

}
