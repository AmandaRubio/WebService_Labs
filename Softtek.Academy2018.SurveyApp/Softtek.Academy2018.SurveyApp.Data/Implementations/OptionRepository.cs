﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionRepository : IOptionRepository
    {
        public OptionRepository()
        {
        }

        public int Add(Option option)
        {
            using(var context = new SuerveyDbContext())
            {
                context.Options.Add(option);
                context.SaveChanges();
                return option.Id;
            }
        }
        
        public bool Delete(Option option)
        {
            using (var context = new SuerveyDbContext())
            {
                Option opt = context.Options.AsNoTracking().SingleOrDefault(o => o.Id == option.Id);
                if (opt == null) return false;
                context.Entry(opt).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }

        public bool Exist(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Options.Any(x => x.Id == id);
            }
        }

        public Option Get(int id)
        {
            using(var context = new SuerveyDbContext())
            {
                return context.Options.FirstOrDefault(o => o.Id == id);
            }
        }

        public bool Update(Option option)
        {
            using (var context = new SuerveyDbContext())
            {
                Option opt = context.Options.SingleOrDefault(o => o.Id == option.Id);
                if (opt == null) return false;
                opt.Id = option.Id;
                opt.CreatedDate = option.CreatedDate;
                opt.ModifiedDate = option.ModifiedDate;
                //opt.Questions = option.Questions;
                opt.Text = option.Text;

                context.Entry(opt).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
