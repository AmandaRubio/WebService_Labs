﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionRepository : IQuestionRepository
    {
        public int Add(Question question)
        {
            using (var context = new SuerveyDbContext())
            {
                context.Questions.Add(question);
                context.SaveChanges();
                return  question.Id;
            }
        }

        public bool Delete(Question question)
        {
            using (var context = new SuerveyDbContext())
            {
                Question qst = context.Questions.AsNoTracking().SingleOrDefault(q => q.Id == question.Id);
                if (qst == null) return false;
                context.Entry(qst).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }

        public bool Exist(int id)
        {
            using(var context = new SuerveyDbContext())
            {
                return context.Questions.Any(x => x.Id == id);
            }
        }

        public Question Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Questions.FirstOrDefault(q => q.Id == id);
            }
        }

        public bool Update(Question question)
        {
            using (var context = new SuerveyDbContext())
            {
                Question qst = context.Questions.SingleOrDefault(o => o.Id == question.Id);
                if (qst == null) return false;
                qst.Id = question.Id;
                qst.IsActive = question.IsActive;
                qst.Text = question.Text;
                qst.QuestionTypeId = question.QuestionTypeId;

                context.Entry(qst).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
