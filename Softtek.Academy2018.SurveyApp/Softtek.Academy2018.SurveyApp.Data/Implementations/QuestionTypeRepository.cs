﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionTypeRepository : IQuestionTypeRepository
    {
        public int Add(QuestionType questionType)
        {
            using (var context = new SuerveyDbContext())
            {
                context.QuetionTypes.Add(questionType);
                context.SaveChanges();
                return questionType.Id;
            }
        }

        

        public bool Exist(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.QuetionTypes.Any(x => x.Id == id);
            }
        }

        public QuestionType Get(int id)
        {
            using(var context = new SuerveyDbContext())
            {
                return context.QuetionTypes.FirstOrDefault(q => q.Id == id);
            }
        }

        public ICollection<QuestionType> GetAll()
        {
            using(var context = new SuerveyDbContext())
            {
                return context.QuetionTypes.ToList();
            }
        }
    }
}
