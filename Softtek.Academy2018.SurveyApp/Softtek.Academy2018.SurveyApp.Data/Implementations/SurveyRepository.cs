﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyRepository : ISurveyRepository
    {
        public int Add(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                context.Surveys.Add(survey);
                context.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Survey s = context.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == survey.Id);
                if (s == null) return false;
                context.Entry(s).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }

        public bool Exist(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.Any(x => x.Id == id);
            }
        }

        public Survey Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.FirstOrDefault(q => q.Id == id);
            }
        }

        public bool Update(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Survey s = context.Surveys.SingleOrDefault(o => o.Id == survey.Id);
                if (s == null) return false;
                s.Title = survey.Title;
                s.Description = survey.Description;

                context.Entry(s).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
